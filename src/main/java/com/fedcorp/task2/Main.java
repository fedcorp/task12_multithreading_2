package com.fedcorp.task2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        PipeCircle pc = new PipeCircle();
        ExecutorService servise = Executors.newFixedThreadPool(2);
        servise.submit(()->pc.readFromConsole());
        servise.submit(()->pc.displayOnConsole());
        servise.shutdown();

    }
}
