package com.fedcorp.task2;

import java.io.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;

public class PipeCircle {

    ArrayBlockingQueue<String> bq;

    public PipeCircle() {
        bq = new ArrayBlockingQueue<>(2);
    }

    public void readFromConsole() {

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String tmp = "";
            do {
                try {
                    tmp = br.readLine();
                    bq.put(tmp);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (!tmp.equals("q"));
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void displayOnConsole() {

            String tmp="";

            do {
                try {
                    tmp = bq.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print(tmp);
                System.out.println();

            } while (!(tmp.toLowerCase().equals("q")));

        }
    }


