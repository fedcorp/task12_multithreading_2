package com.fedcorp.task1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchroDemoModified {
    Lock lock = new ReentrantLock();

    private Object o1 = new Object();
    private Object o2 = new Object();
    private Object o3 = new Object();

    public void method1(){
        lock.lock();
        try {
            for (int i = 0; i <= 100; i++) {
                System.out.print("Method 1 " + i + " ");
            }
            System.out.println();
        }finally{lock.unlock();}

    }
    public void method2(){
        lock.lock();
        try {
            for (int i = 0; i <= 100; i++) {
                System.out.print("Method 2 " + i + " ");
            }
            System.out.println();
        }finally{lock.unlock();}
    }
    public void method3(){
        lock.lock();
        try {
            for (int i = 0; i <= 100; i++) {
                System.out.print("Method 3 " + i + " ");
            }
            System.out.println();
        }finally{lock.unlock();}
    }

}
